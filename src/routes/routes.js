// Importação dos controllers existentes 
const { Router } = require('express');
const UserController = require('../controllers/UserController');
const TitleController = require('../controllers/TitleController');
const CommentController = require('../controllers/CommentController');

// Criando a instância router
const router = Router();

// Rotas para CRUD de Usuário
router.get('/users',UserController.index);
router.get('/users/:id',UserController.show);
router.get('/userComments/:id',UserController.getusercomments);
router.post('/users',UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);

// Rotas para CRUD de Títulos
router.get('/titles',TitleController.index);
router.get('/titles/:id',TitleController.show);
router.get('/searchTitles', TitleController.search);
router.post('/titles',TitleController.create);
router.put('/titles/:id', TitleController.update);
router.delete('/titles/:id', TitleController.destroy);

// Rotas para CRUD de Comentários
router.get('/comments',CommentController.index);
router.get('/comments/:id',CommentController.show);
router.post('/comments',CommentController.create);
router.put('/comments/:id', CommentController.update);
router.delete('/comments/:id', CommentController.destroy);

//Rotas para relacionamento de Comentários

// Adiciona relacionamento com Usuário e Título
router.put('/commentsaddusers/:id', CommentController.addRelationUser);
router.put('/commentsaddtitles/:id', CommentController.addRelationTitle);
// Remove relacionamento com Usuário e Título
router.delete('/commentsremoveusers/:id', CommentController.removeRelationUser);
router.delete('/commentsremovetitles/:id', CommentController.removeRelationTitle);

//Rotas para relacionamento entre Usuários
router.put('/follow/:id', UserController.follow);
router.put('/unfollow/:id', UserController.unfollow);
router.get('/listFollowing/:id',UserController.list_following);
router.get('/listFollowers/:id',UserController.list_followers);

// Exportação das rotas criadas
module.exports = router;

