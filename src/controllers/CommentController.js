// Importação das models existentes e framework sequelize
const { response } = require('express');
const User = require('../models/User');
const Title = require('../models/Title');
const Comment = require('../models/Comment');

// Criação da Rota que retorna todos os comentários do banco de dados
const index = async(req,res) => {
    try {
        const comments = await Comment.findAll();
        return res.status(200).json({comments});
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que retorna um único comentário específico do banco de dados
const show = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        return res.status(200).json({comment});
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que cria novos comentários no banco de dados
const create = async(req,res) => {
    try{
          const comment = await Comment.create(req.body);
          return res.status(201).json({message: "Comentário cadastrado com sucesso!", comment: comment});
      }catch(err){
          res.status(500).json({error: err});
      }
};

// Criação da Rota que atualiza atributos de um comentário do banco de dados
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Comment.update(req.body, {where: {id: id}});
        if(updated) {
            const comment = await Comment.findByPk(id);
            return res.status(200).send(comment);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Comentário não encontrado");
    }
};

// Criação da Rota que deleta um comentário específico do banco de dados
// Antes de remover um comentário, é necessário remover, caso exista, a relação entre o comentário e o título
// para que o atributo score (da entidade title) não seja prejudicado. 
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Comment.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Comentário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Comentário não encontrado.");
    }
};

// Criação da Rota que adiciona relacionamento de um comentário com um usuário
const addRelationUser = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        await comment.setUser(user);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que remove relacionamento de um comentário com um usuário
const removeRelationUser = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        await comment.setUser(null);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que adiciona relacionamento de um comentário com um título
// Ao adicionar uma relação entre comentário e título, o atributo 'score' (da entidade title) é
// atualizado automaticamente utilizando title_rating(da entidade comment)
const addRelationTitle = async(req,res) => {
    const {id} = req.params;
    try {
        const title = await Title.findByPk(req.body.TitleId);
        const comment = await Comment.findByPk(id);
        const atual_score = title.score;
        const count = await Comment.count({
          where: {TitleId: req.body.TitleId}
        });

        const atual = parseFloat(atual_score*count);
        const ponto_novo = parseFloat(comment.title_rating);
        const count_novo = parseInt(count+1);
        const new_score = (atual+ponto_novo)/count_novo;

        const [updated] = await Title.update({score: new_score}, {where: {id: req.body.TitleId}});
        if(!updated){
          throw new Error();
        }

        await comment.setTitle(title);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que remove relacionamento de um comentário com um título
// Ao remover uma relação entre comentário e título, o atributo 'score' (da entidade title) é
// atualizado automaticamente utilizando title_rating(da entidade comment)
const removeRelationTitle = async(req,res) => {
    const {id} = req.params;
    try {
        const title = await Title.findByPk(req.body.TitleId);
        const comment = await Comment.findByPk(id);
        const atual_score = title.score;
        const count = await Comment.count({
          where: {TitleId: req.body.TitleId}
        });

        const atual = parseFloat(atual_score*count);
        const ponto_novo = parseFloat(comment.title_rating);
        const count_novo = parseInt(count-1);
        const new_score = (atual-ponto_novo)/count_novo;

        const [updated] = await Title.update({score: new_score}, {where: {id: req.body.TitleId}});
        if(!updated){
          throw new Error();
        }
        await comment.setTitle(null);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

// Exportação da CRUD e relacionamentos criados acima para routes
module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addRelationUser,
    removeRelationUser,
    addRelationTitle,
    removeRelationTitle
};