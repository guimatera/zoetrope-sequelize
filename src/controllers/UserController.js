// Importação das models existentes e framework sequelize
const { response } = require('express');
const User = require('../models/User');
const Title = require('../models/Title');
const Comment = require('../models/Comment');

// Criação da Rota que retorna todos os usuários do banco de dados
const index = async(req,res) => {
    try {
        const users = await User.findAll({
            include: [
                'following',
                'followed'
            ]
        });
        return res.status(200).json({users});
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que retorna um único usuário específico do banco de dados
const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({user});
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que retorna os comentários de um único usuário específico do banco de dados
const getusercomments = async(req,res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id, {
            include: [{
                model: Comment
            }]
        });
        const Comments = user.Comments;
        return res.status(200).json({Comments});
    }
    catch(err){
        return res.status(500).json({err});
    }
}

// Criação da Rota que cria novos usuários no banco de dados
const create = async(req,res) => {
    try{
          const user = await User.create(req.body);
          return res.status(201).json({message: "Usuário cadastrado com sucesso!", user: user});
      }catch(err){
          res.status(500).json({error: err});
      }
};

// Criação da Rota que atualiza atributos de um usuário do banco de dados
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};

// Criação da Rota que deleta um usuário específico do banco de dados
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuário não encontrado.");
    }
};

// Criação da Rota que cria uma relação de amizade entre dois usuários do banco de dados 
const follow = async(req,res) => {
    const {id} = req.params;
    try {
        const user_following = await User.findByPk(id);
        const user_followed  = await User.findByPk(req.body.userId);
        await user_following.addFollowing(user_followed);
        return res.status(200).json(user_followed);
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que remove uma relação de amizade pré-existente entre dois usuários do banco de dados 
const unfollow = async(req,res) => {
    const {id} = req.params;
    try {
        const user_following = await User.findByPk(id);
        const user_followed  = await User.findByPk(req.body.UserId);
        await user_following.removeFollowing(user_followed);
        return res.status(200).json(user_followed);
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que mostra uma lista de quantos usuários um usuário específico do banco de dados está seguindo
const list_following = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const list_followings  = await user.getFollowing();
        return res.status(200).json({list_followings});
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que mostra uma lista de quantos seguidores um usuário específico do banco de dados possui
const list_followers = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const list_followers  = await user.getFollowed();
        return res.status(200).json({list_followers});
    }catch(err){
        return res.status(500).json({err});
    }
};


// Exportação da CRUD criada acima para routes
module.exports = {
    index,
    show,
    getusercomments,
    create,
    update,
    destroy,
    follow,
    unfollow,
    list_following,
    list_followers
};
