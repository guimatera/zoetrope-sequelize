// Importação do framework sequelize
const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

// Declaração de atributos da model Comment
const Comment = sequelize.define('Comment', {
    content:{
        type: DataTypes.STRING,
        allowNull: false
    }, 

    date_hour:{
        type: DataTypes.DATE,
        allowNull: false
    },

    title_rating:{
        type: DataTypes.FLOAT,
        allow_null: false
    },

}, 

// Banco de dados cria apenas colunas com os atributos declarados acima,
// mais id e, se houver, foreign keys
{
     timestamps: false
});

// Declaração do tipo de associação entre as models
Comment.associate = function(models) {
    Comment.belongsTo(models.User);
    Comment.belongsTo(models.Title);
}

// Exportação de comment para as controllers
module.exports = Comment;