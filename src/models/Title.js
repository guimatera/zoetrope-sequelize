// Importação do framework sequelize
const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

// Declaração de atributos da model Title
const Title = sequelize.define('Title', {
    name:{
        type: DataTypes.STRING,
        allowNull:false
    },

    description:{
        type: DataTypes.STRING,
        allowNull: false
    },

    kind:{
        type: DataTypes.STRING,
        allowNull: false
    },

    score:{
        type: DataTypes.FLOAT
    },

    tags:{
        type: DataTypes.STRING,
        allowNull: false
    },

    publish_date:{
        type: DataTypes.DATE,
        allowNull: false
    },

    release_date:{
        type: DataTypes.DATEONLY,
        allowNull: false
    }

},
/*
// Banco de dados cria apenas colunas com os atributos declarados acima,
// mais id e, se houver, foreign keys
{
     timestamps: false
     
}*/);

// Declaração do tipo de associação entre as models
Title.associate = function(models) {
    Title.hasMany(models.Comment);
}

// Exportação de title para as controllers
module.exports = Title;