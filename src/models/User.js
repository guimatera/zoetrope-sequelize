 // Importação da framework sequelize
const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

// Declaração de atributos da model User
const User = sequelize.define('User', {
    username:{
        type: DataTypes.STRING,
        allowNull: false
    }, 

    email:{
        type: DataTypes.STRING,
        allowNull: false
    },

    first_name:{
        type: DataTypes.STRING,
        allowNull: false
    },

    last_name:{
        type: DataTypes.STRING,
        allowNull: false
    },

    birth_date:{
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    telephone:{
        type: DataTypes.STRING,
        allowNull: false
    },

    password:{
        type: DataTypes.STRING,
        allowNull: false
    }


// Banco de dados cria apenas colunas com os atributos declarados acima,
// mais id e, se houver, foreign keys
/*
{
     timestamps: false
*/
});


// Declaração do tipo de associação entre as models
User.associate = function(models) {
    User.hasMany(models.Comment);
    User.belongsToMany(models.User, {through: 'Follow', as: 'following', foreignKey: 'followingId'});
    User.belongsToMany(models.User, {through: 'Follow', as: 'followed', foreignKey: 'followedId'});
}

//Exportação de user para os controllers
module.exports = User;