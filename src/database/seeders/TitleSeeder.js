const Title = require("../../models/Title");
const faker = require('faker-br');

 const seedTitle = async function () {
   try {
     await Title.sync({ force: true });
     const titles = [];

     for (let i = 0; i < 10; i++) {

      let title = await Title.create({
        name: faker.lorem.word(),
        description: faker.lorem.text(),
        kind: faker.lorem.word(),
        tags: faker.lorem.word(),
        score: faker.random.number(0),
        publish_date: new Date(),
        release_date: new Date(),
        createdAt: new Date(),
        updatedAt: new Date()
      });

    }

  } catch (err) { console.log(err +'!'); }
}

module.exports = seedTitle;