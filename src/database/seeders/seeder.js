require('../../config/dotenv')();
require('../../config/sequelize');

seedUser = require('./UserSeeder');
seedComment = require('./CommentSeeder');
seedTitle = require('./TitleSeeder');

(async () => {
  try {
    await seedUser();
    await seedTitle();
    await seedComment();

  } catch(err) { console.log(err) }
})();
